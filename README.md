# Bazoj - Types #

#### Read about Bazoj ####

## TYPES ##

Types is a library for simplifying type checking.

### How do I get set up? ###

Install using:

```
npm install bazoj-types
```

Then import in the code using:

```
import types as <local name> from 'bazoj-types';
// or
const <local name> = require('bazoj-types');
```