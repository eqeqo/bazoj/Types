function isBool(variable) {
	return typeof variable === "boolean";
}

function isNumber(variable) {
	return typeof variable === "number" && !isNaN(variable);
}

function isInt(variable) {
	return isNumber(variable) && Number.isInteger(variable);
}

function isFloat(variable) {
	return isNumber(variable) && !isInt(variable);
}

function isString(variable) {
	return typeof variable === "string";
}

function isArray(variable) {
	return Array.isArray(variable);
}

function isObject(variable) {
	return typeof variable === "object"
			&& !isArray(variable)
			&& variable !== null;
};

function isEmpty(variable) {
	return variable === null
			|| variable === undefined
			|| variable === ""
			|| (isArray(variable) && variable.length === 0)
			|| (isObject(variable) && Object.keys(variable).length === 0);
}

module.exports = {
	isBool,
	isNumber,
	isInt,
	isFloat,
	isString,
	isArray,
	isObject,
	isEmpty,
};
