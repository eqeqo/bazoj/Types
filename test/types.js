const {
	isBool,
	isNumber,
	isInt,
	isFloat,
	isString,
	isArray,
	isObject,
	isEmpty,
} = require('../index.js');

// Function to run tests and display results
function runTest(testFunction, expected, testName) {
	const result = testFunction();
	if (result === expected) {
		console.log(`${testName} - OK`);
	}
	else {
		console.log(`${testName} - FAILED. Expected: ${expected}, Got: ${result}`);
	}
}

// Test cases for each function
runTest(() => isBool(true), true, 'isBoolean test 1');
runTest(() => isBool(false), true, 'isBoolean test 2');
runTest(() => isBool('true'), false, 'isBoolean test 3');

runTest(() => isInt(42), true, 'isInt test 1');
runTest(() => isInt(42.5), false, 'isInt test 2');
runTest(() => isInt('42'), false, 'isInt test 3');

runTest(() => isFloat(42.5), true, 'isFloat test 1');
runTest(() => isFloat(42), false, 'isFloat test 2');
runTest(() => isFloat('42.5'), false, 'isFloat test 3');

runTest(() => isString('Hello'), true, 'isString test 1');
runTest(() => isString(42), false, 'isString test 2');

runTest(() => isArray([1, 2, 3]), true, 'isArray test 1');
runTest(() => isArray('test'), false, 'isArray test 2');

runTest(() => isObject({}), true, 'isObject test 1');
runTest(() => isObject(null), false, 'isObject test 2');
runTest(() => isObject('string'), false, 'isObject test 3');

runTest(() => isNumber(42), true, 'isNumber test 1');
runTest(() => isNumber('42'), false, 'isNumber test 2');
runTest(() => isNumber(NaN), false, 'isNumber test 3');

runTest(() => isEmpty(null), true, 'isEmpty test 1');
runTest(() => isEmpty(undefined), true, 'isEmpty test 2');
runTest(() => isEmpty(''), true, 'isEmpty test 3');
runTest(() => isEmpty([]), true, 'isEmpty test 4');
runTest(() => isEmpty({}), true, 'isEmpty test 5');

runTest(() => isEmpty(0), false, 'isEmpty test 1');
runTest(() => isEmpty('Hello'), false, 'isEmpty test 2');
runTest(() => isEmpty([1, 2, 3]), false, 'isEmpty test 3');
runTest(() => isEmpty({ key: 'value' }), false, 'isEmpty test 4');
