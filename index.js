const {
	isBool,
	isNumber,
	isInt,
	isFloat,
	isString,
	isArray,
	isObject,
	isEmpty,
} = require ('./src/types.js');

module.exports = {
	isBool,
	isNumber,
	isInt,
	isFloat,
	isString,
	isArray,
	isObject,
	isEmpty,
};
